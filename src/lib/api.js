import axios from "axios";
//must parse in seperate function
export const parseXml = (data) => {
    const parser = new DOMParser();

    const xml = parser.parseFromString(data, "text/xml");
    console.log(xml);
    //console.log(xml.getElementsByTagName("image")[0].getElementsByTagName("url")[0].innerHTML)
    //console.log(xml.getElementsByTagName("item")[0].getElementsByTagName("title"));
    //console.log(xml.getElementsByTagName("enclosure")[0].attributes.url.value)
    //setUrl(xml.getElementsByTagName("enclosure")[0].attributes.url.value);

    return xml;
}

export const getRss = (url) => {
    return axios.get(url).then((res) => res.data);
}
