import {Avatar, Box, Button, Paper, Stack, Typography} from "@mui/material";
import {useEffect, useState} from "react";

const Show = ({podcast, subscribed, setSubscribed}) => {
    const [alreadySubscribed, setAlreadySubscribed] = useState(subscribed.find(subscribed => subscribed.getElementsByTagName("title")[0].textContent === podcast.getElementsByTagName("title")[0].textContent));

    useEffect(() => {
        setAlreadySubscribed(subscribed.find(subscribed => subscribed.getElementsByTagName("title")[0].textContent === podcast.getElementsByTagName("title")[0].textContent));
    }, [podcast, subscribed])

    return (
        <Box sx={{padding: 2}}>
            <Paper elevation={2} sx={{padding: 2}}>
                <Stack spacing={2}>
                    <Stack direction={"row"} spacing={2}>
                        <Avatar
                            src={podcast.getElementsByTagName("image")[0].getElementsByTagName("url")[0].innerHTML}
                            variant={"rounded"}
                            sx={{
                                width: 105,
                                height: 105

                            }}>

                        </Avatar>
                        <Typography variant={"h4"}>{podcast.getElementsByTagName("title")[0].textContent}</Typography>
                    </Stack>
                    <Typography>{podcast.getElementsByTagName("description")[0].textContent}</Typography>
                </Stack>
                {alreadySubscribed ? <></> :
                    <Button onClick={() => setSubscribed([...subscribed, podcast])}>Subscribe</Button>
                }
            </Paper>
        </Box>
    );
}

export default Show;