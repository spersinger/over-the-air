import {Box, Button, Paper, Typography} from "@mui/material";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';

const Episode = ({episode, setEpisode}) => {
    return (
        <Box sx={{padding: 2}}>
            <Paper elevation={3} sx={{padding: 2}}>
                <Typography variant={"h5"}>{episode.getElementsByTagName("title")[0].innerHTML}</Typography>
                <Typography>{episode.getElementsByTagName("itunes:summary")[0].innerHTML}</Typography>
                <Button sx={{marginTop: 2}} startIcon={<PlayArrowIcon/>} variant="contained"
                        onClick={() => setEpisode({
                            "url": episode.getElementsByTagName("enclosure")[0].attributes.url.value,
                            "title": episode.getElementsByTagName("title")[0].innerHTML,
                            "summary": episode.getElementsByTagName("itunes:summary")[0].innerHTML
                        })
                        }
                >Play</Button>
            </Paper>
        </Box>
    );
}

export default Episode;