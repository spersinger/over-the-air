import {
    AppBar, Avatar,
    Backdrop,
    Box,
    Button,
    CircularProgress,
    Grid,
    Stack,
    TextField,
    Toolbar, Typography
} from "@mui/material";
import Show from "./Show";
import {useEffect, useState} from "react";

import H5AudioPlayer from "react-h5-audio-player";
//import 'react-h5-audio-player/lib/styles.css';
import '../styles/audioplayer.css'
import {useQuery} from "@tanstack/react-query";
import {getRss, parseXml} from "../lib/api";
import SubscribedGrid from "./SubscribedGrid";
import EpisodeList from "./EpisodeList";


const MainPage = () => {
    const [rssFeed, setRssFeed] = useState("https://feeds.simplecast.com/54nAGcIl")
    const [subscribed, setSubscribed] = useState([])
    const [podcast, setPodcast] = useState();
    const [episode, setEpisode] = useState({title: "", url: "", summary: ""});
    const [episodeArray, setEpisodeArray] = useState();
    const [rssu, setRssu] = useState();
    const [page, setPage] = useState(0);

    const queryRss = useQuery(['rssfeed', rssFeed], () => getRss(rssFeed));

    useEffect(() => {
        if (queryRss.isSuccess && subscribed !== [] && podcast) {
            setEpisodeArray(Array.from(podcast.getElementsByTagName("item")));
        }
    }, [podcast, queryRss.isSuccess, subscribed])

    useEffect(() => {
        document.title = "Over the Air: " + episode.title ? episode.title : "";
    }, [episode])

    useEffect(() => {
        if (queryRss.isSuccess) {
            const parsedPod = parseXml(queryRss.data)
            const array = Array.from(parsedPod.getElementsByTagName("item"))
            setPodcast(parsedPod);
            setEpisodeArray(array);
        }
    }, [queryRss.isSuccess, queryRss.data])


    if (queryRss.isLoading) return (
        <Backdrop
            sx={{color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1}}
            open={queryRss.isLoading}
        >
            <CircularProgress color="inherit"/>
        </Backdrop>
    )
    if (queryRss.isError) return <span>error</span>

    if (queryRss.isSuccess) {
        if (podcast) {
            return (
                <Box>
                    <Stack spacing={2}>
                        <Grid container>
                            <Grid item xs={12} md={7}>
                                <Show subscribed={subscribed} setSubscribed={setSubscribed} podcast={podcast}/>
                                <Box sx={{padding: 2}}>
                                    <Stack spacing={2} direction={"row"}>
                                        <TextField onBlur={(e => setRssu(e.target.value))}/>
                                        <Button onClick={() => setRssFeed(rssu)}>Change</Button>
                                    </Stack>
                                </Box>
                                <SubscribedGrid subscribed={subscribed} setSubscribed={setSubscribed} setPodcast={setPodcast}/>
                            </Grid>
                            <Grid item xs={12} md={5}>
                                <EpisodeList setEpisode={setEpisode} episodeArray={episodeArray} page={page} setPage={setPage} />
                            </Grid>
                        </Grid>
                    </Stack>
                    {/* Play bar */}
                    <AppBar position="fixed" color="primary" sx={{top: 'auto', bottom: 0}}>
                        <Box sx={{marginLeft: 3, marginTop: 1}}>
                            <Typography variant={"h5"}>{episode.title ? episode.title : ""}</Typography>
                        </Box>
                        <Toolbar>
                            <Avatar
                                src={podcast.getElementsByTagName("image")[0].getElementsByTagName("url")[0].innerHTML}
                                variant={"rounded"}
                                sx={{
                                    width: 64,
                                    height: 64
                                }}>

                            </Avatar>
                            <H5AudioPlayer src={episode.url}/>
                        </Toolbar>
                    </AppBar>
                </Box>
            );
        }
    }

    return <span></span>
}

export default MainPage;