import {Avatar, Box, Button, Grid, Paper, Stack, Typography} from "@mui/material";

const SubscribedGrid = ({subscribed, setPodcast, setSubscribed}) => {
    return (
    <Box x={{padding: 2}}>
        <Grid container>
            {subscribed.map(podcast =>
                <Grid key={podcast.getElementsByTagName("title")[0].textContent} item xs={12} md={4} sx={{padding: 2}}>
                    <Paper elevation={2} sx={{padding: 2}} onClick={() => setPodcast(podcast)}>
                        <Stack direction={"row"} spacing={2}>
                            <Avatar
                                src={podcast.getElementsByTagName("image")[0].getElementsByTagName("url")[0].innerHTML}
                                variant={"rounded"}
                                sx={{
                                    width: 64,
                                    height: 64
                                }}>

                            </Avatar>
                            <Typography variant={"h5"}>{podcast.getElementsByTagName("title")[0].textContent}</Typography>
                        </Stack>
                        <Button sx={{marginTop: 2}} onClick={() => setSubscribed(subscribed.filter(subscribedPodcast => subscribedPodcast.getElementsByTagName("title")[0].innerHTML !== podcast.getElementsByTagName("title")[0].textContent))}>Unsubscribe</Button>
                    </Paper>
                </Grid>
            )}
        </Grid>
    </Box>
    );
}

export default SubscribedGrid;