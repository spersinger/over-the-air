import {Box, Grid, Pagination} from "@mui/material";
import Episode from "./Episode";

const EpisodeList = ({episodeArray, setEpisode, page, setPage}) => {
    return (
        <Box>
            {episodeArray.slice(page*10, (page+1)*10).map(episode =>
                <Episode key={episode.getElementsByTagName("guid")[0].innerHTML} setEpisode={setEpisode}
                         episode={episode}/>
            )}
            <Grid container
                  spacing={0}
                  direction="column"
                  alignItems="center"
                  justify="center"
                  style={{ minHeight: '15vh' }}>
                <Pagination count={Math.ceil(episodeArray.length / 10)} onClick={(event) => setPage(parseInt(event.target.innerText)-1)} page={page+1} hideNextButton hidePrevButton/>
            </Grid>
        </Box>
    )
}

export default EpisodeList;