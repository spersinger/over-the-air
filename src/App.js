import MainPage from "./Components/MainPage";

const App = () => {
    return (
        <div>
            <MainPage />
        </div>
    );
}

export default App;